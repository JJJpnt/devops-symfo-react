<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Entity\Post;
use App\Repository\TagRepository;
use App\Repository\PostRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api", name="api_posts_")
 */
class PostController extends AbstractController
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/posts", name="index", methods={"GET"})
     * 
     */
    public function index(PostRepository $pr): Response
    {
        $posts = $pr->findAll();

        $json = $this->serializer->serialize($posts, "json", ["groups" => "post:read"]);

        return new Response($json, 200, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/posts/{slug}", name="post", methods={"GET", "PATCH"}): Response
     */
    public function post(Post $post, Request $request, ManagerRegistry $mr, TagRepository $tr)
    {
        if ($request->isMethod("GET")) {
            $json = $this->serializer->serialize($post, "json", ["groups" => "post:read"]);
        } else if ($request->isMethod("PATCH")) {
            $res = $request->getContent();

            $this->serializer->deserialize(
                $res,
                Post::class,
                "json",
                [
                    "groups" => "editPost",
                    "object_to_populate" => $post,
                ]
            );

            $data = json_decode($res, true);
            $tags = $data["tags"];

            // on supprime les tags qui ne sont pas dans la liste des tags du post
            foreach ($post->getTags() as $tag) {
                if (in_array($tag->getName(), $tags) === false) {
                    $post->removeTag($tag);
                }
            }

            foreach ($tags as $tag) {
                $existingTag = $tr->findOneBy($tag);
                // si le tag n'existe pas, on le crée
                dump("tag");
                if (!$existingTag) {
                    $newTag = new Tag();
                    $newTag->setName($tag['name']);
                    $post->addTag($newTag);
                    $mr->getManager()->persist($newTag);
                } else {
                    // si le tag n'est pas dans l'array des tags du post, on l'ajoute
                    if (!in_array($existingTag, $post->getTags() )) {
                        $post->addTag($existingTag);
                    }
                    
                }
            }

            $mr->getManager()->flush();

            $json = $this->serializer->serialize($post, "json", ["groups" => "post:read"]);

            return new Response($json, 200, ["groups" => "post:read"], ["Content-Type" => "application/json"]);
        }

        return new Response($json, 200, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/tags", name="tags", methods={"GET"}): Response
     */
    public function tags(TagRepository $tr)
    {
        $tags = $tr->findAll();
        $json = $this->serializer->serialize($tags, "json", ["groups" => "tag:read"]);

        return new Response($json, 200, ["Content-Type" => "application/json"]);
    }
}
