<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class FrontController extends AbstractController
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    /**
     * @Route("/", name="app_home")
     */
    public function index(PostRepository $pr): Response
    {
        // $posts = $pr->getPostsTitle();
        $posts = $pr->findAll();

        // dd($posts);

        return $this->render('front/index.html.twig', [
            'controller_name' => 'FrontController',
            "posts" => $posts,
        ]);
    }

    /**
     * @Route("/posts/{slug}", name="app_post")
     * @ParamConverter("post", options={"mapping": {"slug": "slug"}})
     */
    public function post(Post $post, Request $request, ManagerRegistry $mr): Response
    {
        // equivalent du param converter
        // $post = $pr->findOneBy(['slug' => $slug]);

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $em = $mr->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('app_post', ['slug' => $post->getSlug()]);
        }

        return $this->render('front/post.html.twig', [
            'controller_name' => 'FrontController',
            "post" => $post,
            "form" => $form->createView(),
        ]);
    }
}
