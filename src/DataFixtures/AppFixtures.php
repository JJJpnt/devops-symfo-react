<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
  private $slugger;
  private $encoder;

  public function __construct(SluggerInterface $slugger, UserPasswordEncoderInterface $encoder)
  {
    $this->slugger = $slugger;
    $this->encoder = $encoder;
  }

  public function load(ObjectManager $manager)
  {
    // TODO remplir bdd
    $user = new User();

    $user->setEmail('john@gmail.fr');
    $password = $this->encoder->encodePassword($user, '123456');
    $user->setPassword($password);
    $user->setName('John');
    $user->setUsername('john');

    $manager->persist($user);

    for ($i = 0; $i < 10; $i++) {
      $post = new Post();
      $post->setUser($user);
      $post->setTitle('post ' . $i);
      $post->setSlug($this->slugger->slug($post->getTitle()));
      $post->setBody("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra magna et felis faucibus, a tempus ligula porta. Nulla volutpat placerat nulla vel ultrices. Ut et magna finibus, lacinia leo ut, eleifend augue. Vestibulum imperdiet augue nec velit rutrum ultrices. Cras non sodales purus. Nulla malesuada vehicula magna sodales ultricies. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas eros mauris, tincidunt id augue eu, dapibus porta odio. Morbi ante est, dapibus nec cursus in, rutrum ut libero. Phasellus varius nisl magna, at ultricies ante scelerisque id. Praesent viverra malesuada tortor sit amet egestas. Nulla quis dapibus nisi. Mauris vel gravida arcu. Duis sed augue eu justo porttitor iaculis pellentesque non ligula. Praesent gravida efficitur tellus, at scelerisque mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam aliquet pellentesque nibh, vel malesuada nisi sagittis sed. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut ac aliquet purus. Aliquam non imperdiet libero, id finibus metus. Nulla volutpat ornare mi non malesuada. Aliquam lectus nisl, venenatis rhoncus consequat a, dapibus id dolor. Aenean ultricies quam nisl, vel egestas ex fermentum at. Aenean elementum, nisl non blandit ullamcorper, nulla quam volutpat velit, in tristique dolor nulla vitae turpis. Ut cursus justo justo, sit amet imperdiet turpis blandit nec. Curabitur porttitor a purus nec iaculis. Maecenas dapibus, lorem a scelerisque dictum, mi quam placerat dolor, vitae condimentum sapien ligula lacinia nunc.");
      $manager->persist($post);
    }

    $manager->flush();
  }
}
