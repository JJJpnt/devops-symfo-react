<?php

namespace App\Form;

use App\Entity\Tag;
use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class)
            ->add('slug', TextType::class)
            ->add('body', TextareaType::class)
            ->add('user', EntityType::class, [
                'class' => 'App\Entity\User',
                'choice_label' => 'name',
            ])
            ->add('tags', EntityType::class, [
                'class' => 'App\Entity\Tag',
                'multiple' => true,
                'expanded' => true,
                'choice_label' => function (Tag $tag) {
                    return $tag->getName() . ' (' . $tag->getPosts()->count() . ')';
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
