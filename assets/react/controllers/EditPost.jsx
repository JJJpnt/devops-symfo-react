import React, { useState } from 'react'

// 1. j'instancie le composant EditPost en récupérant les props passé par le twig
const EditPost = ({ postId, postBody, postSlug, url }) => {

  // 2. je déclare un state pour gérer le mode d'affichage du composant
  const [isEditing, setIsEditing] = useState(false)

  const handleClick = () => {
    setIsEditing(!isEditing)
  }

  const handleForm = (event) => {
    event.preventDefault();
    const formData = new FormData(event.target)

    fetch(url, {
      method: "PATCH",
      body: JSON.stringify({
        body: formData.get('body'),
        title: null
      }),
      headers: {
        contentType: "application/json",
      }
    })
      .then(response => response.json())
      .then(data => {
        console.log(data)
      })
      .catch(error => console.log(error))
  }



  return (
    <div className="p-4">
      {isEditing ? (
        <div>
          <h6>en edition</h6>
          <form onSubmit={handleForm}>
            <textarea defaultValue={postBody} name="body" rows="10" className="w-full" />
            <button type="submit" className="bg-indigo-500 px-4 py-1 text-white rounded-sm hover:bg-indigo-700 transition">Finir</button>
          </form>
        </div>
      ) : (
        <button type="button" onClick={handleClick} className="bg-indigo-500 px-4 py-1 text-white rounded-sm hover:bg-indigo-700 transition">Editer</button>
      )}
    </div>
  )
}

export default EditPost
