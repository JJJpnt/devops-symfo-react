import React, { useCallback, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { fetchPosts } from "./AdminInterface";

const Spinner = (
  <svg className="animate-spin h-5 w-5 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
    <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
    <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
  </svg>
)


const PostForm = ({ slug }) => {
  const [post, setPost] = useState(null);
  const [tags, setTags] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const { register, handleSubmit, formState: { errors }, reset } = useForm();

  const updateTags = async () => {
    const response = await fetch("https://127.0.0.1:8000/api/tags")
    const data = await response.json();
    return setTags(data);
  }

  const onSubmit = (data, event) => {
    setIsLoading(true);
    const method = post ? "PATCH" : "POST";
    const tagsInputs = event.target.querySelectorAll("input[type=checkbox]:checked");
    data.tags = [];
    tagsInputs.forEach((tag) => {
      console.log(tag.value)
      data.tags.push({ name: tag.value })
    })
    // récupère les tags saisis dans le champs input, les sépare et les ajoute à la liste des tags
    const newTags = event.target.querySelector("input[name=new-tags]").value.split(",");
    newTags.forEach((tag) => {
      if (tag.trim() !== "")
        data.tags.push({ name: tag.trim() })
    })
    // supprime la propriété user de l'objet data
    delete data.user;
    fetchPosts(post?.slug, { method: method, body: data })
      .then((data) => {
        setPost(data);
        updateTags();
        return data;
      })
      .finally((data) => {
        setIsLoading(false);
        reset(data);
      })
  }

  useEffect(() => {
    setIsLoading(true);
    if (slug) {
      fetchPosts(slug)
        .then((data) => {
          setPost(data);
        })
        .finally(() => {
          setIsLoading(false);
        })
    }
    updateTags()
      .finally(() => {
        setIsLoading(false);
      })
  }, [])

  useEffect(() => {
    if (post) {
      reset(post);
    }
  }, [post])

  return (
    <div className="relative">
      {isLoading && <div className="absolute inset-0 flex items-center justify-center bg-white/60 transition">{Spinner} Chargement...</div>}
      <form onSubmit={handleSubmit(onSubmit)} className="grid space-y-4">
        <input type="text" {...register("slug", { required: { value: true, message: "Le champs ne peut-être vide." } })} />
        {errors.slug && <span>{errors.slug?.message}</span>}
        <input type="text" {...register("title", { required: { value: true, message: "Le champs ne peut-être vide." } })} />
        <textarea {...register("body")} rows={10} />
        <ul className="text-sm flex gap-2 items-baseline flex-wrap">
          {tags.map((tag, idx) => {
            console.log("jsx", post)
            const isChecked = post.tags.find((postTag) => postTag.name === tag.name) ? true : false
            return (
              <Tag key={idx} name={tag.name} checked={isChecked} />
            )
          })}
        </ul>
        <div className="grid space-y-2">
          <div className="grid">
            <label htmlFor="input-new-tags">Ajouter de nouveaux tags *</label>
            <small className="italic">* séparer les tags par une virgule.</small>
          </div>
          <input type="text" name="new-tags" id="input-new-tags" />
        </div>
        <button type="submit" className="mx-auto hover:bg-indigo-700 bg-indigo-500 transition text-sm py-1 px-2 text-white rounded-sm">Soumettre</button>
      </form>
    </div>
  )
}

const Tag = ({ name, checked }) => {
  return (
    <li className="flex items-center gap-1">
      <input type="checkbox" id={name} name="tags.name" value={name} defaultChecked={checked} />
      <label htmlFor={name}>{name}</label>
    </li>
  )
}

export default PostForm;
