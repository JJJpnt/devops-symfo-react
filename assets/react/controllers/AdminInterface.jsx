import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import PostForm from "./PostForm";

export const fetchPosts = async (slug = null, options) => {
  const url = slug ? `https://127.0.0.1:8000/api/posts/${slug}` : "https://127.0.0.1:8000/api/posts";
  const response = await fetch(url, {
    method: options?.method || "GET",
    headers: {
      "Content-Type": "application/json",
    },
    ...options?.body && { body: JSON.stringify(options.body) }
  });
  const data = await response.json();
  return data;
};

const AdminInterface = () => {
  const [posts, setPosts] = useState([]);
  const [selectedPost, setSelectedPost] = useState(null);
  const [addPost, setAddPost] = useState(false);

  useEffect(() => {
    fetchPosts().then((data) => {
      setPosts(data);
    });
  }, []);

  return (
    <div>
      <div className="container">
        <h1 className="text-indigo-600 font-medium text-2xl py-4">Admin Interface</h1>
        <button className="text-indigo-500 text-sm mb-4 rounded-sm hover:text-indigo-700 transition block w-fit" onClick={() => setAddPost(true)}>Ajouter un article</button>
        {!selectedPost && (
          <ul className="grid bg-gray-100 shadow divide-gray-300 divide-solid divide-y cursor-pointer mb-4">
            {posts.map((post, idx) => (
              <li key={idx} className="p-3 transition group hover:bg-gray-200 flex justify-between" onClick={() => setSelectedPost(post.slug)}>
                <p>{post.title}</p>
                <button className="group-hover:bg-indigo-700 bg-indigo-500 transition text-sm py-1 px-2 text-white rounded-sm">Modifier</button>
              </li>
            ))}
          </ul>
        )}
        {selectedPost && (
          <>
            <button className="text-indigo-500 text-sm mb-4 rounded-sm hover:text-indigo-700 transition" onClick={() => setSelectedPost(null)}>Retour</button>
            <PostForm slug={selectedPost} />
          </>
        )}
        {addPost && <PostForm />}
      </div>
    </div>
  );
}

export default AdminInterface;
