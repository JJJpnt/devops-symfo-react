<?php

namespace App\Tests;

use App\Entity\Tag;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testTag()
    {
        $demo = new Tag();

        $demo->setName('test');

        $this->assertTrue($demo->getName() === 'test');
    }
}